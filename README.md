`joba` submits (Sun Grid Engine) jobs and
puts their output in a fresh directory.
For array jobs all of the tasks in the array use the same output directory.

    joba [options] [script]

_options_ are options to `qsub`;

_script_ is the script to run.

The output directory is created in the current directory and
named after the job id.
